from sqlalchemy import Column, DateTime, String, Integer, ForeignKey, func, select
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine


Base = declarative_base()


class Department(Base):
    __tablename__ = 'department'
    id = Column(Integer, primary_key=True)
    name = Column(String)


class Employee(Base):
    __tablename__ = 'employee'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    # Use default=func.now() to set the default hiring time
    # of an Employee to be the current time when an
    # Employee record was created
    hired_on = Column(DateTime, default=func.now())
    department_id = Column(Integer, ForeignKey('department.id'))
    # Use cascade='delete,all' to propagate the deletion of a Department onto its Employees
    department = relationship(
        Department,
        backref=backref('employees',
                        uselist=True,
                        cascade='delete,all'))


engine = create_engine('sqlite:///orm_in_detail.sqlite', echo=True, future=True)


session = sessionmaker()
session.configure(bind=engine)
Base.metadata.create_all(engine)


with session() as sess:
    with session.begin():
        dep1 = Department(name='IT')
        dep2 = Department(name='Cleaning')
        user1 = Employee(name='Pavel', department=dep1)
        user2 = Employee(name='Maria', department=dep2)
        sess.add(dep1)
        sess.add(dep2)
        sess.add(user1)
        sess.add(user2)
    with session.begin():
        result = sess.execute(select(Employee).filter_by(name='Maria')).all()
        print(type(result))
        print(len(result))
        print(result[0][0].id, result[0][0].name, result[0][0].department.name)
    #  More examples here: https://docs.sqlalchemy.org/en/14/orm/session_basics.html
