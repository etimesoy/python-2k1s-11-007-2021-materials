import sqlite3


con = sqlite3.connect('example.db')
try:
    cur1 = con.cursor()

    cur1.execute('''
        CREATE TABLE IF NOT EXISTS stocks
        (date text, trans text, symbol text, qty real, price real)
    ''')
    cur1.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")
    con.commit()

    cur2 = con.cursor()
    cur2.execute('SELECT * FROM stocks')
    for row in cur2.fetchall():
        print(row)
finally:
    con.close()
