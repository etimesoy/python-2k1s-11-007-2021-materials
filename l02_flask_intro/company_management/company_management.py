from flask import Blueprint, render_template

company_management_blueprint = Blueprint('company_management', __name__, template_folder='templates')


@company_management_blueprint.route('/', methods=('GET',))
def main_page():
    return render_template('company_management/main_page.html')
